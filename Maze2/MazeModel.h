#pragma once
#ifndef __MAZEMODEL_H__
#define __MAZEMODEL_H__
#include <stdlib.h>
#include <Windows.h>
#include "maze.h"

#define MAX_STOP_COUNT 300
#define MAX_REVISIT_COUNT 50
#define MAX_STEPS 10000

Maze *MazeModel_random(unsigned width, unsigned height, float probability) {
	Maze *m = Maze_malloc(width, height);

	// generate a random block
	for (unsigned i = 2; i < width; i++)
		for (unsigned j = 2; j < height; j++)
			Maze_set(m, i, j, (rand() < (probability * RAND_MAX)) ? MAZE_BLOCK : MAZE_EMPTY);

	// set wall
	for (unsigned i = 1; i <= height; i++) {
		Maze_set(m, 1, i, MAZE_WALL);
		Maze_set(m, width, i, MAZE_WALL);
	}
	for (unsigned i = 1; i <= width; i++) {
		Maze_set(m, i, 1, MAZE_WALL);
		Maze_set(m, i, height, MAZE_WALL);
	}

	// make exits
	Maze_set(m, 1, 3, MAZE_EXIT);
	Maze_set(m, 2, 3, MAZE_EMPTY);
	Maze_set(m, width, height - 2, MAZE_EXIT);
	Maze_set(m, width - 1, height - 2, MAZE_EMPTY);
	m->entrance.x = 1;
	m->entrance.y = 3;
	m->exit.x = width;
	m->exit.y = height - 2;
	return m;
}

Maze *MazeModel_empty(unsigned width, unsigned height, float probability) {
	Maze *m = Maze_malloc(width, height);

	// generate a random block
	for (unsigned i = 2; i < width; i++)
		for (unsigned j = 2; j < height; j++)
			Maze_set(m, i, j, 0);

	// set wall
	for (unsigned i = 1; i <= height; i++) {
		Maze_set(m, 1, i, MAZE_WALL);
		Maze_set(m, width, i, MAZE_WALL);
	}
	for (unsigned i = 1; i <= width; i++) {
		Maze_set(m, i, 1, MAZE_WALL);
		Maze_set(m, i, height, MAZE_WALL);
	}

	// make exits
	Maze_set(m, 1, 3, MAZE_EXIT);
	Maze_set(m, 2, 3, MAZE_EMPTY);
	Maze_set(m, width, height - 2, MAZE_EXIT);
	Maze_set(m, width - 1, height - 2, MAZE_EMPTY);
	m->entrance.x = 1;
	m->entrance.y = 3;
	m->exit.x = width;
	m->exit.y = height - 2;
	return m;
}

bool MazeModel_isBlock(Maze_datatype d) {
	return (d == MAZE_WALL) || (d == MAZE_BLOCK);
}

bool MazeModel_isWalked(Maze_datatype d) {
	return (d == MAZE_WALKER) || (d == MAZE_WALKER_HEAD) || (d == MAZE_EXIT);// || (d == MAZE_SUCC) || d == (MAZE_FAIL);
}

char *MazeModel_walkIterator(Maze *m, DWORD sleep_time, Point(*walk)(Maze *m, Point pos), void(*redraw)(Maze *m, Point pos), void(*succ)(Maze *m, Point pos), void(*fail)(Maze *m, Point pos)) {
	walk(NULL, (Point) { 0, 0 }); // initialize static vars
	static char *s;
	if (s != NULL) free(s);
	s = malloc((m->width * m->height * 10 + 128) * sizeof(char));
	s[0] = 0;
	Point current_position = m->entrance;
	unsigned step_count = 0;
	unsigned stop_count = 0;
	unsigned revisited_count = 0;
	while (!Point_isEqual(current_position, m->exit)) {
		step_count++;
		Point new_position = walk(m, current_position);
		if (Point_isEqual(new_position, current_position) || MazeModel_isWalked(Maze_get(m, new_position.x, new_position.y))) {
			stop_count++; revisited_count++;
		}
		else stop_count = 0;
		if (step_count >= MAX_STEPS || stop_count >= MAX_STOP_COUNT || revisited_count >= MAX_REVISIT_COUNT || Point_isEqual((Point) { 0, 0 }, new_position)/*error*/) {
			fail(m, current_position);
			return NULL;
			return;
		}
		current_position = new_position;
		redraw(m, current_position);
		sprintf(s + strlen(s), "->(%u, %u)", current_position.x, current_position.y);
		printf("(%u, %u)", m->entrance.x, m->entrance.y);
		printf("%s\n", s);
		Sleep(sleep_time);
	}
	succ(m, current_position);
	return s;
}

int MazeModel_routeIterator(Maze *m, DWORD sleep_time, Point(*routeSearch)(Maze *m, Point pos), void(*redraw)(Maze *m, Point pos), void(*succ)(Maze *m, Point pos), void(*fail)(Maze *m, Point pos)) {
	routeSearch(NULL, (Point) { 0, 0 }); // initialize static vars

	Point current_position = m->entrance;
	unsigned step_count = 0;
	unsigned stop_count = 0;
	unsigned revisited_count = 0;
	while (!Point_isEqual(current_position, m->exit)) {
		step_count++;
		Point new_position = routeSearch(m, current_position);
		if (Point_isEqual(new_position, current_position) || MazeModel_isWalked(Maze_get(m, new_position.x, new_position.y))) {
			stop_count++; revisited_count++;
		}
		else stop_count = 0;
		if (step_count >= MAX_STEPS || stop_count >= MAX_STOP_COUNT || revisited_count >= MAX_REVISIT_COUNT || Point_isEqual((Point) { 0, 0 }, new_position)/*error*/) {
			fail(m, current_position);
			return EXIT_FAILURE;
			return;
		}
		current_position = new_position;
		redraw(m, current_position);
		Sleep(sleep_time);
	}
	succ(m, current_position);
	return EXIT_SUCCESS;
}
#endif