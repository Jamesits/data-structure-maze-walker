#pragma once
#ifndef __MAZE_H__
#define __MAZE_H__
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#define MAZE_WALL 219
#define MAZE_BLOCK 178
#define MAZE_EMPTY 32
#define MAZE_EXIT 249
#define MAZE_WALKER 1
#define MAZE_WALKER_HEAD 2
#define MAZE_SUCC 2
#define MAZE_FAIL 15
#define MAZE_ARROW_UP 24
#define MAZE_ARROW_DOWN 25
#define MAZE_ARROW_RIGHT 26
#define MAZE_ARROW_LEFT 27

typedef enum { DIRECTION_LEFT, DIRECTION_UP, DIRECTION_RIGHT, DIRECTION_DOWN } Direction;

Direction Direction_turnLeft(Direction d) {
	d -= 1;
	if (d < 0) d += 4;
	return d;
}

Direction Direction_turnRight(Direction d) {
	return (d + 1) % 4;
}

typedef int Maze_datatype;

typedef struct {
	unsigned x;
	unsigned y;
	//Direction d;
} Point;

typedef struct {
	unsigned width;
	unsigned height;
	Point entrance;
	Point exit;
	Maze_datatype *data;
} Maze;

unsigned __Maze_get_offset(Maze *m, unsigned width, unsigned height) {
	return (height - 1) * m->width + width - 1;
}

bool Point_isEqual(Point a, Point b) {
	return (a.x == b.x) && (a.y == b.y);
}

Maze *Maze_malloc(unsigned width, unsigned height) {
	Maze *m = (Maze*)malloc(sizeof(Maze));
	m->width = width;
	m->height = height;
	m->data = (Maze_datatype *)malloc(height * width * sizeof(Maze_datatype));
	return m;
}

void Maze_free(Maze *m) {
	free(m->data);
}

Maze_datatype Maze_get(Maze *m, unsigned width, unsigned height) {
	if (width > m->width || height > m->height || width < 1 || height < 1) { return MAZE_WALL; }
	return m->data[__Maze_get_offset(m, width, height)];
}

void Maze_set(Maze *m, unsigned width, unsigned height, Maze_datatype d) {
	if (width > m->width || height > m->height || width < 1 || height < 1) { return; }
	m->data[__Maze_get_offset(m, width, height)] = d;
}

Maze_datatype Maze_getNearBlock(Maze *m, Point p, Direction direction) {
	Maze_datatype d;
	switch (direction) {
	case DIRECTION_DOWN:
		d = Maze_get(m, p.x, p.y + 1); break;
	case DIRECTION_LEFT:
		d = Maze_get(m, p.x - 1, p.y); break;
	case DIRECTION_RIGHT:
		d = Maze_get(m, p.x + 1, p.y); break;
	case DIRECTION_UP:
		d = Maze_get(m, p.x, p.y - 1); break;
	}
	return d;
}

Point Point_moveTo(Point p, Direction direction) {
	switch (direction) {
	case DIRECTION_DOWN:
		p.y += 1; break;
	case DIRECTION_LEFT:
		p.x -= 1; break;
	case DIRECTION_RIGHT:
		p.x += 1; break;
	case DIRECTION_UP:
		p.y -= 1; break;
	}
	//p.d = direction;
	return p;
}

void Maze_clear(Maze *m) {
	for (unsigned i = 1; i <= m->height; i++) {
		for (unsigned j = 1; j <= m->width; j++) {
			Maze_datatype d = Maze_get(m, j, i);
			if ((d == MAZE_WALKER) || (d == MAZE_WALKER_HEAD)) {
				Maze_set(m, j, i, MAZE_EMPTY);
			}
		}
	}
}

#endif