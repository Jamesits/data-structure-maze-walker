#pragma once
#ifndef __WALK_RULES_H__
#define __WALK_RULES_H__
#include "maze.h"
#include "MazeModel.h"
#include "MazeView.h"

#define MAX_ROUNDS 1

Point walk_left(Maze *m, Point pos) {
	static Direction last_direction = DIRECTION_RIGHT;
	static unsigned rounds = 0;
	if (m == NULL) { last_direction = DIRECTION_RIGHT; rounds = 0; return (Point) { 0, 0 }; }

	Maze_datatype t = Maze_getNearBlock(m, pos, Direction_turnLeft(last_direction));
	if (!MazeModel_isBlock(t)) {
		last_direction = Direction_turnLeft(last_direction);
	}
	else if (!MazeModel_isBlock(Maze_getNearBlock(m, pos, last_direction))) {
		// don't turn
	}
	else if (!MazeModel_isBlock(Maze_getNearBlock(m, pos, Direction_turnRight(last_direction)))) {
		last_direction = Direction_turnRight(last_direction);
	}
	else {
		// nowhere to go
		last_direction = Direction_turnRight(last_direction);
		last_direction = Direction_turnRight(last_direction);
		return pos;
	}
	return Point_moveTo(pos, last_direction);
}

Point walk_right(Maze *m, Point pos) {
	const Direction initial_direction = DIRECTION_RIGHT;
	static Direction last_direction;
	static int turns = 0;
	if (m == NULL) { last_direction = initial_direction; turns = 0; return (Point) {0, 0}; }
	Direction new_direction;
	if (!MazeModel_isBlock(Maze_getNearBlock(m, pos, Direction_turnRight(last_direction)))) {
		new_direction = Direction_turnRight(last_direction);
		turns--;
	}
	else if (!MazeModel_isBlock(Maze_getNearBlock(m, pos, last_direction))) {
		new_direction = last_direction;
		// don't turn
	}
	else if (!MazeModel_isBlock(Maze_getNearBlock(m, pos, Direction_turnLeft(last_direction)))) {
		// turn left
		new_direction = Direction_turnLeft(last_direction);
		turns++;
	}
	else {
		// turn back and stay
		new_direction = Direction_turnLeft(Direction_turnLeft(last_direction));
		turns += 2;
		//return pos;
	}
	Point new_pos = Point_moveTo(pos, new_direction);
	if (!MazeModel_isWalked(Maze_get(m, new_pos.x, new_pos.y))) turns = 0;
	if ((abs(turns) > MAX_ROUNDS * 4) || Point_isEqual(new_pos, m->entrance)) return (Point) { 0, 0 }; // died
	last_direction = new_direction;
	return new_pos;
}

#endif