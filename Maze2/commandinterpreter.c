#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <ctype.h>
#include "commandinterpreter.h"
#include "maze.h"
#include "MazeModel.h"
#include "MazeView.h"
#include "walk_rules.h"

Maze *m = NULL;
char *s = NULL;

typedef struct {
	char *command;
	int(*eval)(char *s);
	char *help;
} cmd_dict;

cmd_dict commands[] = {
	{ "CLEAR",	cls,		"" },
	{ "CLS",	cls,		"Clears the screen." },
	{ "DUMP",	save,		"Save current maze data to a dump file." },
	{ "EXIT",	quit,		"Quits this software (command interpreter)." },
	{ "EXPORT",	exportm,		"Export maze run result." },
	{ "H",		help,		"" },
	{ "HELP",	help,		"Provides Help information for commands available." },
	{ "IMPORT",	import,		"Import a user-defined maze." },
	{ "LOAD",	load,		"Load a maze from a dump file." },
	{ "MAN",	help,		"" },
	{ "PRINT",	print,		"Print current maze." },
	{ "RANDOM",	random,		"Randomly generate a maze." },
	{ "RUN",	run,		"Solve the maze." },
	{ "QUIT",	quit,		"" },
	{ "UNAME",	version,	"" },
	{ "VER",	version,	"Displays the software version." },
	{ "VERSION",version,	"" },
};

static bool mute = false;

bool is_arg_present(char *arg) {
	if (arg == NULL) return false;
	while (*arg != 0 && isspace(*arg)) arg++;
	if (*arg == 0) return false;
	return true;
}

int help(char *arg) {
	for (int i = 0; i<sizeof commands / sizeof commands[0]; ++i) {
		if (strlen(commands[i].help)) printf("%s\t\t%s\n", commands[i].command, commands[i].help);
	}
	return EXIT_SUCCESS;
}

int version(char *arg) {
	puts("Maze2\n(c)2015 James Swineson. All rigits reserved.\nPowered by libJInterpreter.\n");
	return EXIT_SUCCESS;
}

int cls(char *arg) {
	system("@cls||clear");
	puts("");
	return EXIT_SUCCESS;
}

int quit(char *arg) {
	puts("Quitting...");
	return EXIT_FAILURE;
}

int random(char *arg) {
	if (m != NULL) Maze_free(m);
	unsigned width = 20, height = 20;
	float percent = 0.27;
	if (is_arg_present(arg)) {
		sscanf(arg, "%u%u%f", &width, &height, &percent);
	}
	m = MazeModel_random(width, height, percent);
	return EXIT_SUCCESS;
}

int print(char *arg) {
	showMaze(m);
	return EXIT_SUCCESS;
}

int run(char *arg) {
	Maze_clear(m);
	Point(*walk)(Maze *m, Point pos) = walk_right;
	if (is_arg_present(arg)) {
		if (!strcmp(arg, "left")) walk = walk_left;
		if (!strcmp(arg, "right")) walk = walk_right;
	}
	s = MazeModel_walkIterator(m, 200, walk, showMaze_ongoing, showMaze_succ, showMaze_failed);
	if (s != NULL) {
		printf("(%u, %u)", m->entrance.x, m->entrance.y);
		printf("%s\n", s);
	}
	return EXIT_SUCCESS;
}

int save(char *arg) {
	if (!is_arg_present(arg)) {
		fprintf(stderr, "Error: Filename not provided.\n");
		return EXIT_SUCCESS;
	}
	char filename[1024 + 1];
	if (sscanf(arg, "%1024s", filename) != 1) {
		fprintf(stderr, "Error: Invalid filename.\n");
		return EXIT_SUCCESS;
	}

	// strip '"' between file path
	while (*arg == '"') arg++;
	char *p = arg;
	while (*(p++) != 0) {
		if (*p == '"') {
			*p = 0;
			break;
		}
	}
	
	FILE *dest = fopen(arg, "w");
	if (dest == NULL) {
		fprintf(stderr, "Error: Failed to open file '%s'.\n", arg);
		return EXIT_SUCCESS;
	}
	else {
		fprintf(dest, "%u %u %u %u %u %u ", m->width, m->height, m->entrance.x, m->entrance.y, m->exit.x, m->exit.y);
		for (int i = 1; i <= m->width; i++) {
			for (int j = 1; j <= m->height; j++) {
				fprintf(dest, "%d ", Maze_get(m, i, j));
			}
		}
		
		fclose(dest);
		printf("Exported.\n");
		return EXIT_SUCCESS;
	}
	
	return EXIT_SUCCESS;
}

int load(char *arg) {
	if (!is_arg_present(arg)) {
		fprintf(stderr, "Error: Filename not provided.\n");
		return EXIT_SUCCESS;
	}
	char filename[1024 + 1];
	if (sscanf(arg, "%1024s", filename) != 1) {
		fprintf(stderr, "Error: Invalid filename.\n");
		return EXIT_SUCCESS;
	}
	
	// strip '"' between file path
	while (*arg == '"') arg++;
	char *p = arg;
	while (*(p++) != 0) {
		if (*p == '"') {
			*p = 0;
			break;
		}
	}

	FILE *src = fopen(arg, "r");
	if (src == NULL) {
		fprintf(stderr, "Error: Failed to open file '%s'.\n", arg);
		return EXIT_SUCCESS;
	}
	else {
		if (m != NULL) Maze_free(m);
		unsigned width;
		unsigned height;
		fscanf(src, "%u%u", &width, &height);
		m = Maze_malloc(width, height);
		fscanf(src, "%u%u%u%u", &(m->entrance.x), &(m->entrance.y), &(m->exit.x), &(m->exit.y));
		for (int i = 1; i <= m->width; i++) {
			for (int j = 1; j <= m->height; j++) {
				Maze_datatype d;
				fscanf(src, "%d", &d);
				Maze_set(m, i, j, (MazeModel_isWalked(d) && (d != MAZE_EXIT))?MAZE_EMPTY:d);
			}
		}

		fclose(src);
		printf("Imported.\n");
		return EXIT_SUCCESS;
	}
	
	return EXIT_SUCCESS;
}

int import(char *arg) {
	if (!is_arg_present(arg)) {
		fprintf(stderr, "Error: Filename not provided.\n");
		return EXIT_SUCCESS;
	}
	char filename[1024 + 1];
	if (sscanf(arg, "%1024s", filename) != 1) {
		fprintf(stderr, "Error: Invalid filename.\n");
		return EXIT_SUCCESS;
	}

	// strip '"' between file path
	while (*arg == '"') arg++;
	char *p = arg;
	while (*(p++) != 0) {
		if (*p == '"') {
			*p = 0;
			break;
		}
	}

	FILE *src = fopen(arg, "r");
	if (src == NULL) {
		fprintf(stderr, "Error: Failed to open file '%s'.\n", arg);
		return EXIT_SUCCESS;
	}
	else {
		if (m != NULL) Maze_free(m);
		unsigned width;
		unsigned height;
		fscanf(src, "%u%u", &width, &height);
		m = Maze_malloc(width, height);
		//fscanf(src, "%u%u%u%u", &(m->entrance.x), &(m->entrance.y), &(m->exit.x), &(m->exit.y));
		for (int i = 1; i <= m->height; i++) {
			for (int j = 1; j <= m->width; j++) {
				Maze_datatype d;
				fscanf(src, "%1d", &d);
				Maze_set(m, j, i, d ? MAZE_BLOCK : MAZE_EMPTY);
			}
		}

		// set wall
		for (unsigned i = 1; i <= height; i++) {
			Maze_set(m, 1, i, MAZE_WALL);
			Maze_set(m, width, i, MAZE_WALL);
		}
		for (unsigned i = 1; i <= width; i++) {
			Maze_set(m, i, 1, MAZE_WALL);
			Maze_set(m, i, height, MAZE_WALL);
		}

		// make exits
		Maze_set(m, 1, 3, MAZE_EXIT);
		Maze_set(m, 2, 3, MAZE_EMPTY);
		Maze_set(m, width, height - 2, MAZE_EXIT);
		Maze_set(m, width - 1, height - 2, MAZE_EMPTY);
		m->entrance.x = 1;
		m->entrance.y = 3;
		m->exit.x = width;
		m->exit.y = height - 2;

		fclose(src);
		printf("Imported.\n");
		return EXIT_SUCCESS;
	}

	return EXIT_SUCCESS;
}

int exportm(char *arg) {
	if (!is_arg_present(arg)) {
		fprintf(stderr, "Error: Filename not provided.\n");
		return EXIT_SUCCESS;
	}
	char filename[1024 + 1];
	if (sscanf(arg, "%1024s", filename) != 1) {
		fprintf(stderr, "Error: Invalid filename.\n");
		return EXIT_SUCCESS;
	}

	// strip '"' between file path
	while (*arg == '"') arg++;
	char *p = arg;
	while (*(p++) != 0) {
		if (*p == '"') {
			*p = 0;
			break;
		}
	}

	FILE *dest = fopen(arg, "w");
	if (dest == NULL) {
		fprintf(stderr, "Error: Failed to open file '%s'.\n", arg);
		return EXIT_SUCCESS;
	}
	else if (s != NULL) {
		fprintf(dest, "%u %u\n\n", m->width, m->height);
		fshowMaze(dest, m);
		fprintf(dest, "\n(%u, %u)", m->entrance.x, m->entrance.y);
		fprintf(dest, "%s\n", s);
		fclose(dest);
		printf("Exported.\n");
		return EXIT_SUCCESS;
	}
	else {
		puts("RUN first, please!");
		return EXIT_SUCCESS;
	}

	return EXIT_SUCCESS;
}

int runcommand(char *s)
{
	char temp[20];
	if (sscanf(s, "%20s", temp) != EOF) {
		temp[19] = 0;
		for (int i = 0; i < strlen(temp); ++i) temp[i] = toupper(temp[i]);
		for (int i = 0; i < sizeof commands / sizeof commands[0]; ++i) {
			if (strcmp(temp, commands[i].command) == 0) {
				char *t = s + strlen(commands[i].command);
				if (isspace(*t)) t++;
				if ( *t == 0 ) t = NULL;
				return (*(commands[i].eval))(t);
			}
		}
		sscanf(s, "%20s", temp);
		printf("'%s' is not recognized as an internal command.\n", temp);
	}
	return EXIT_SUCCESS;
}

int loop()
{
	random(NULL);
	char cmd[1024 + 1] = { 0 };
	do {
		printf("Maze2 >");
	} while (gets_s(cmd, 1024)!= NULL && !runcommand(cmd));
	return EXIT_SUCCESS;
}