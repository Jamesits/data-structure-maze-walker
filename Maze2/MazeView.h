#pragma once
#ifndef __MAZEVIEW_H__
#define __MAZEVIEW_H__
#include <stdio.h>
#include "maze.h"

void showMaze(Maze *m) {
	system("cls");
	for (unsigned i = 1; i <= m->height; i++) {
		for (unsigned j = 1; j <= m->width; j++) {
			putc( Maze_get(m, j, i), stdout);  //219, 176-178, 32
		}
		putc('\n', stdout);
	}
}

void fshowMaze(FILE *f, Maze *m) {
	for (unsigned i = 1; i <= m->height; i++) {
		for (unsigned j = 1; j <= m->width; j++) {
			putc(MazeModel_isBlock(Maze_get(m, j, i)) ? '1' : '0', f);
		}
		putc('\n', f);
	}
}

void showMaze_ongoing(Maze *m, Point p) {
	Maze_set(m, p.x, p.y, Maze_get(m, p.x, p.y) == MAZE_WALKER ? MAZE_WALKER_HEAD : MAZE_WALKER);
	showMaze(m);
}

void showMaze_failed(Maze *m, Point p) {
	Maze_set(m, p.x, p.y, MAZE_FAIL);
	showMaze(m);
	puts("Failed.");
}

void showMaze_succ(Maze *m, Point p) {
	Maze_set(m, p.x, p.y, MAZE_SUCC);
	showMaze(m);
	puts("Successed.");
}

void showMazeRoute_failed(Maze *m, Point p) {
	printf("(%u, %u) ", p.x, p.y);
	//puts("Failed.");
}

void showMazeRoute_ongoing(Maze *m, Point p) {
	fprintf(stdout, "(%u, %u) ", p.x, p.y);
}

void showMazeRoute_succ(Maze *m, Point p) {
	printf("(%u, %u) ", p.x, p.y);
	//puts("Successed.");
}

#endif